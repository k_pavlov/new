msgid ""
msgstr ""
"Project-Id-Version: cs-cart-latest\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Language-Team: Lithuanian\n"
"Language: lt_LT\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: cs-cart-latest\n"
"X-Crowdin-Language: lt\n"
"X-Crowdin-File: /addons/rss_feed.po\n"
"Last-Translator: cscart <zeke@cs-cart.com>\n"
"PO-Revision-Date: 2016-05-16 10:53-0400\n"

msgctxt "Addons::name::rss_feed"
msgid "RSS feed"
msgstr "RSS informacijos santraukos"

msgctxt "Addons::description::rss_feed"
msgid "Generates RSS feeds for products and blog posts"
msgstr ""

msgctxt "Languages::rss_feed"
msgid "RSS feed"
msgstr "RSS informacijos santraukos"

msgctxt "Languages::feed_title"
msgid "Feed title"
msgstr "Informacijos santraukos"

msgctxt "Languages::feed_description"
msgid "Feed description"
msgstr "Feed Aprašymas"

msgctxt "Languages::rss_sort_by"
msgid "Sort items by"
msgstr "Rūšiuoti elementus pagal"

msgctxt "Languages::rss_display_sku"
msgid "Display SKU"
msgstr "Rodyti SKU"

msgctxt "Languages::rss_display_image"
msgid "Display image"
msgstr "Rodyti paveikslėlį"

msgctxt "Languages::rss_display_price"
msgid "Display gross price"
msgstr "Rodyti bruto kaina"

msgctxt "Languages::rss_display_original_price"
msgid "Display net price"
msgstr "Rodyti kainą"

msgctxt "Languages::rss_display_add_to_cart"
msgid "Display add to cart button"
msgstr "Rodyti įdėti į krepšelį mygtuką"

msgctxt "Languages::rss_created"
msgid "Created"
msgstr "Sukurta"

msgctxt "Languages::rss_updated"
msgid "Updated"
msgstr "Atnaujinta"

msgctxt "Languages::block_rss_feed"
msgid "RSS feed"
msgstr "RSS informacijos santraukos"

msgctxt "Languages::block_rss_feed_description"
msgid "RSS feed subscription icon"
msgstr "RSS informacijos santraukos prenumeratos piktograma"

msgctxt "SettingsSections::rss_feed::general"
msgid "General"
msgstr "Bendras"

msgctxt "SettingsOptions::rss_feed::managing_editor"
msgid "Content editor"
msgstr "Turinio redaktorius"

msgctxt "SettingsOptions::rss_feed::display_rss_feed_in_category"
msgid "Show RSS feed for each category"
msgstr "Rodyti RSS feed kiekvienai kategorijai"

msgctxt "SettingsOptions::rss_feed::category_max_products_items"
msgid "Max product number per category"
msgstr "Max produkto skaičius kiekvienai kategorijai"

msgctxt "SettingsTooltips::rss_feed::managing_editor"
msgid "Enter the content editor's email and real name, e.g. john@example.com (John Doe)"
msgstr "Įveskite turinio redaktoriaus laišką ir vardą , pvz john@example.com (John Doe)"

msgctxt "SettingsTooltips::rss_feed::display_rss_feed_in_category"
msgid "RSS icon near category title."
msgstr "RSS piktograma – kategorijos pavadinimas."

